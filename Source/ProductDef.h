//
//  ProductDef.h
//  DryAged
//
//  Created by Rudolf Leitner on 03/03/17.
//
//

#ifndef ProductDef_h
#define ProductDef_h

//#define DOLOGGING
//#define SHOW_RELOAD_BUTTON

#define PRODUCT_NAME "DryAged"
#define LOGGER_PREFIX "DRAG_"
#define SAMPLEBIN_NAME "DryAged.bin"
//#define CONFIGFILE_NAME "DryAged.xml"
//#define CONFIG_FROM_FILE

#endif /* ProductDef_h */
