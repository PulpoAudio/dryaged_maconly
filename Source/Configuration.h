//
//  Configuration.h
//  DryAged
//
//  Created by Rudolf Leitner on 07/03/17.
//
//

#ifndef Configuration_h
#define Configuration_h

#include <string>
using namespace std;

static const char* configuration = R"zzz(
<?xml version="1.0" ?>
<Instrument>
<Patches>
<Patch name="Dry Aged" displaceSemiTones="12">
<Setup gainDb="0.0" monophonic="false" numVoices="16" pan="0.0" tune="0.0" veloSens="0.75" bevel="1.0"/>
<Layers gainLayer1="0.0" gainLayer2="0.0" gainLayer3="0.0" gainLayer4="0.0"/>
<Pressure controller="1" mode="VELOCITY"/>
<Pitchwheel attackTime="0.0" mode="SEMITONES" offsetTime="0.0" rangeSt="12" releaseTime="0.0"/>
<Legato attackTime="0.0" mode="OFF" offsetTime="0.0" releaseTime="0.0"/>
<Dfd dfd="false" loadFrames="22050" numRingBuffers="96" preloadFrames="44100"/>
<AHDSREnvelope attackTime="0.0" decayTime="0.0" holdTime="0.0" offsetTime="0.0" releaseTime="0.1" sustainDb="0.0"/>
<Keyswitches rangeFromKey="36" rangeToKey="40">
<Keyswitch key="0" name="Bridge" parameter="0" permanent="true" type="SOUNDSET"/>
<Keyswitch key="1" name="Hole" parameter="1" permanent="true" type="SOUNDSET"/>
<Keyswitch key="2" name="Muted" parameter="2" permanent="true" type="SOUNDSET"/>
<Keyswitch key="4" name="Legato" permanent="false" type="SIMPLELEGATO"/>
</Keyswitches>
<Sounds rangeFromKey="43" rangeToKey="86" bevelFromKey="48" bevelToKey="86">
<Sound centerNote="47" layer="0" noteRangeFrom="47" noteRangeTo="47" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSlide_00" velocityRangeFrom="0.66" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSlide_01" velocityRangeFrom="0.33" velocityRangeTo="0.66" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSLide_02" velocityRangeFrom="0.0" velocityRangeTo="0.33" xFadeVelocityTo="0.66"/>
</Sound>

<Sound centerNote="47" layer="1" noteRangeFrom="47" noteRangeTo="47" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSlide_00" velocityRangeFrom="0.66" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSlide_01" velocityRangeFrom="0.33" velocityRangeTo="0.66" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSLide_02" velocityRangeFrom="0.0" velocityRangeTo="0.33" xFadeVelocityTo="0.66"/>
</Sound>


<Sound centerNote="47" layer="0" noteRangeFrom="47" noteRangeTo="47" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSlide_00" velocityRangeFrom="0.66" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSlide_01" velocityRangeFrom="0.33" velocityRangeTo="0.66" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSLide_02" velocityRangeFrom="0.0" velocityRangeTo="0.33" xFadeVelocityTo="0.66"/>
</Sound>

<Sound centerNote="47" layer="1" noteRangeFrom="47" noteRangeTo="47" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSlide_00" velocityRangeFrom="0.66" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSlide_01" velocityRangeFrom="0.33" velocityRangeTo="0.66" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSLide_02" velocityRangeFrom="0.0" velocityRangeTo="0.33" xFadeVelocityTo="0.66"/>
</Sound>

<Sound centerNote="47" layer="0" noteRangeFrom="47" noteRangeTo="47" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSlide_00" velocityRangeFrom="0.66" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSlide_01" velocityRangeFrom="0.33" velocityRangeTo="0.66" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSLide_02" velocityRangeFrom="0.0" velocityRangeTo="0.33" xFadeVelocityTo="0.66"/>
</Sound>

<Sound centerNote="47" layer="1" noteRangeFrom="47" noteRangeTo="47" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSlide_00" velocityRangeFrom="0.66" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSlide_01" velocityRangeFrom="0.33" velocityRangeTo="0.66" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSLide_02" velocityRangeFrom="0.0" velocityRangeTo="0.33" xFadeVelocityTo="0.66"/>
</Sound>


<Sound centerNote="43" layer="0" noteRangeFrom="43" noteRangeTo="43" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="43" layer="1" noteRangeFrom="43" noteRangeTo="43" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="44" layer="0" noteRangeFrom="44" noteRangeTo="44" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_06" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_07" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_08" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_09" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_10" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_11" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="44" layer="1" noteRangeFrom="44" noteRangeTo="44" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_06" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_07" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_08" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_09" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_10" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_11" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="45" layer="0" noteRangeFrom="45" noteRangeTo="45" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_12" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_13" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_14" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_15" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_16" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_17" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="45" layer="1" noteRangeFrom="45" noteRangeTo="45" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_12" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_13" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_14" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_15" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_16" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_17" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="46" layer="0" noteRangeFrom="46" noteRangeTo="46" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_18" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_19" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_20" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_21" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_22" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxStegTap_23" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="46" layer="1" noteRangeFrom="46" noteRangeTo="46" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_19" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_19" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_20" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_21" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_22" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxStegTap_23" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>




<Sound centerNote="43" layer="0" noteRangeFrom="43" noteRangeTo="43" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="43" layer="1" noteRangeFrom="43" noteRangeTo="43" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="44" layer="0" noteRangeFrom="44" noteRangeTo="44" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="44" layer="1" noteRangeFrom="44" noteRangeTo="44" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="45" layer="0" noteRangeFrom="45" noteRangeTo="45" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="45" layer="1" noteRangeFrom="45" noteRangeTo="45" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="46" layer="0" noteRangeFrom="46" noteRangeTo="46" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="46" layer="1" noteRangeFrom="46" noteRangeTo="46" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>


<Sound centerNote="43" layer="0" noteRangeFrom="43" noteRangeTo="43" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="43" layer="1" noteRangeFrom="43" noteRangeTo="43" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="44" layer="0" noteRangeFrom="44" noteRangeTo="44" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="44" layer="1" noteRangeFrom="44" noteRangeTo="44" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="45" layer="0" noteRangeFrom="45" noteRangeTo="45" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="45" layer="1" noteRangeFrom="45" noteRangeTo="45" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="46" layer="0" noteRangeFrom="46" noteRangeTo="46" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitteTap_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="46" layer="1" noteRangeFrom="46" noteRangeTo="46" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitteTap_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>


<Sound centerNote="52" layer="0" noteRangeFrom="48" noteRangeTo="52" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="54" layer="0" noteRangeFrom="53" noteRangeTo="54" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_06" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_07" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_08" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_09" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_10" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_11" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="56" layer="0" noteRangeFrom="55" noteRangeTo="56" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_12" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_13" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_14" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_15" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_16" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_17" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="58" layer="0" noteRangeFrom="57" noteRangeTo="58" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_18" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_19" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_20" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_21" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_22" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_23" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="60" layer="0" noteRangeFrom="59" noteRangeTo="60" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_24" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_25" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_26" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_27" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_28" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_29" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="62" layer="0" noteRangeFrom="61" noteRangeTo="62" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_30" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_31" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_32" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_33" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_34" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_35" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="64" layer="0" noteRangeFrom="63" noteRangeTo="64" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_36" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_37" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_38" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_39" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_40" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_41" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="66" layer="0" noteRangeFrom="65" noteRangeTo="66" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_42" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_43" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_44" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_45" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_46" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_47" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="68" layer="0" noteRangeFrom="67" noteRangeTo="68" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_48" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_49" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_50" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_51" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_52" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_53" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="70" layer="0" noteRangeFrom="69" noteRangeTo="70" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_54" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_55" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_56" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_57" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_58" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_59" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="72" layer="0" noteRangeFrom="71" noteRangeTo="72" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_60" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_61" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_62" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_63" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_64" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_65" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="74" layer="0" noteRangeFrom="73" noteRangeTo="74" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_66" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_67" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_68" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_69" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_70" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_71" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="76" layer="0" noteRangeFrom="75" noteRangeTo="76" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_72" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_73" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_74" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_75" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_76" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_77" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="78" layer="0" noteRangeFrom="77" noteRangeTo="78" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_78" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_79" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_80" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_81" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_82" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_83" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="80" layer="0" noteRangeFrom="79" noteRangeTo="86" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_84" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_85" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_86" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_87" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_88" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxSteg_89" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="52" layer="1" noteRangeFrom="48" noteRangeTo="52" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="54" layer="1" noteRangeFrom="53" noteRangeTo="54" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_06" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_07" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_08" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_09" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_10" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_11" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="56" layer="1" noteRangeFrom="55" noteRangeTo="56" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_12" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_13" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_14" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_15" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_16" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_17" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="58" layer="1" noteRangeFrom="57" noteRangeTo="58" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_18" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_19" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_20" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_21" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_22" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_23" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="60" layer="1" noteRangeFrom="59" noteRangeTo="60" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_24" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_25" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_26" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_27" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_28" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_29" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="62" layer="1" noteRangeFrom="61" noteRangeTo="62" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_30" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_31" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_32" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_33" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_34" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_35" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="64" layer="1" noteRangeFrom="63" noteRangeTo="64" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_36" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_37" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_38" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_39" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_40" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_41" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="66" layer="1" noteRangeFrom="65" noteRangeTo="66" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_42" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_43" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_44" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_45" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_46" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_47" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="68" layer="1" noteRangeFrom="67" noteRangeTo="68" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_48" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_49" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_50" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_51" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_52" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_53" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="70" layer="1" noteRangeFrom="69" noteRangeTo="70" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_54" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_55" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_56" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_57" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_58" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_59" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="72" layer="1" noteRangeFrom="71" noteRangeTo="72" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_60" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_61" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_62" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_63" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_64" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_65" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="74" layer="1" noteRangeFrom="73" noteRangeTo="74" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_66" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_67" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_68" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_69" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_70" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_71" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="76" layer="1" noteRangeFrom="75" noteRangeTo="76" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_72" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_73" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_74" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_75" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_76" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_77" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="78" layer="1" noteRangeFrom="77" noteRangeTo="78" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_78" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_79" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_80" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_81" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_82" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_83" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="80" layer="1" noteRangeFrom="79" noteRangeTo="86" soundSet="0" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_84" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_85" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_86" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_87" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_88" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxSteg_89" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>


<Sound centerNote="52" layer="0" noteRangeFrom="48" noteRangeTo="52" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="54" layer="0" noteRangeFrom="53" noteRangeTo="54" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_06" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_07" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_08" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_09" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_10" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_11" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="56" layer="0" noteRangeFrom="55" noteRangeTo="56" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_12" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_13" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_14" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_15" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_16" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_17" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="58" layer="0" noteRangeFrom="57" noteRangeTo="58" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_18" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_19" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_20" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_21" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_22" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_23" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="60" layer="0" noteRangeFrom="59" noteRangeTo="60" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_24" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_25" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_26" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_27" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_28" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_29" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="62" layer="0" noteRangeFrom="61" noteRangeTo="62" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_30" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_31" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_32" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_33" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_34" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_35" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="64" layer="0" noteRangeFrom="63" noteRangeTo="64" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_36" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_37" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_38" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_39" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_40" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_41" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="66" layer="0" noteRangeFrom="65" noteRangeTo="66" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_42" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_43" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_44" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_45" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_46" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_47" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="68" layer="0" noteRangeFrom="67" noteRangeTo="68" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_48" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_49" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_50" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_51" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_52" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_53" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="70" layer="0" noteRangeFrom="69" noteRangeTo="70" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_54" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_55" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_56" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_57" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_58" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_59" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="72" layer="0" noteRangeFrom="71" noteRangeTo="72" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_60" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_61" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_62" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_63" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_64" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_65" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="74" layer="0" noteRangeFrom="73" noteRangeTo="74" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_66" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_67" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_68" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_69" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_70" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_71" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="76" layer="0" noteRangeFrom="75" noteRangeTo="76" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_72" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_73" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_74" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_75" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_76" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_77" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="78" layer="0" noteRangeFrom="77" noteRangeTo="78" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_78" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_79" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_80" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_81" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_82" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_83" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="80" layer="0" noteRangeFrom="79" noteRangeTo="86" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_84" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_85" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_86" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_87" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_88" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMitte_89" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="52" layer="1" noteRangeFrom="48" noteRangeTo="52" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="54" layer="1" noteRangeFrom="53" noteRangeTo="54" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_06" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_07" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_08" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_09" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_10" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_11" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="56" layer="1" noteRangeFrom="55" noteRangeTo="56" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_12" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_13" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_14" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_15" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_16" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_17" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="58" layer="1" noteRangeFrom="57" noteRangeTo="58" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_18" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_19" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_20" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_21" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_22" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_23" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="60" layer="1" noteRangeFrom="59" noteRangeTo="60" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_24" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_25" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_26" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_27" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_28" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_29" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="62" layer="1" noteRangeFrom="61" noteRangeTo="62" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_30" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_31" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_32" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_33" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_34" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_35" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="64" layer="1" noteRangeFrom="63" noteRangeTo="64" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_36" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_37" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_38" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_39" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_40" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_41" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="66" layer="1" noteRangeFrom="65" noteRangeTo="66" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_42" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_43" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_44" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_45" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_46" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_47" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="68" layer="1" noteRangeFrom="67" noteRangeTo="68" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_48" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_49" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_50" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_51" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_52" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_53" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="70" layer="1" noteRangeFrom="69" noteRangeTo="70" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_54" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_55" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_56" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_57" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_58" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_59" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="72" layer="1" noteRangeFrom="71" noteRangeTo="72" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_60" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_61" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_62" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_63" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_64" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_65" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="74" layer="1" noteRangeFrom="73" noteRangeTo="74" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_66" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_67" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_68" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_69" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_70" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_71" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="76" layer="1" noteRangeFrom="75" noteRangeTo="76" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_72" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_73" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_74" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_75" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_76" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_77" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="78" layer="1" noteRangeFrom="77" noteRangeTo="78" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_78" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_79" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_80" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_81" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_82" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_83" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="80" layer="1" noteRangeFrom="79" noteRangeTo="86" soundSet="1" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_84" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_85" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_86" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_87" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_88" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMitte_89" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>




<Sound centerNote="52" layer="0" noteRangeFrom="48" noteRangeTo="52" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_0" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="54" layer="0" noteRangeFrom="53" noteRangeTo="54" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_06" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_07" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_08" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_09" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_10" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_11" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="56" layer="0" noteRangeFrom="55" noteRangeTo="56" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_12" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_13" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_14" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_15" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_16" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_17" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="58" layer="0" noteRangeFrom="57" noteRangeTo="58" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_18" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_19" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_20" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_21" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_22" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_23" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="60" layer="0" noteRangeFrom="59" noteRangeTo="60" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_24" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_25" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_26" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_27" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_28" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_29" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="62" layer="0" noteRangeFrom="61" noteRangeTo="62" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_30" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_31" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_32" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_33" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_34" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_35" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="64" layer="0" noteRangeFrom="63" noteRangeTo="64" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_36" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_37" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_38" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_39" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_40" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_41" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="66" layer="0" noteRangeFrom="65" noteRangeTo="66" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_42" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_43" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_44" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_45" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_46" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_47" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="68" layer="0" noteRangeFrom="67" noteRangeTo="68" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_48" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_49" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_50" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_51" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_52" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_53" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="70" layer="0" noteRangeFrom="69" noteRangeTo="70" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_54" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_55" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_56" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_57" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_58" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_59" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="72" layer="0" noteRangeFrom="71" noteRangeTo="72" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_60" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_61" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_62" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_63" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_64" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_65" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="74" layer="0" noteRangeFrom="73" noteRangeTo="74" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_66" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_67" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_68" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_69" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_70" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_71" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="76" layer="0" noteRangeFrom="75" noteRangeTo="76" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_72" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_73" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_74" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_75" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_76" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_77" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="78" layer="0" noteRangeFrom="77" noteRangeTo="78" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_78" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_79" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_80" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_81" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_82" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_83" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="80" layer="0" noteRangeFrom="79" noteRangeTo="86" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_84" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_85" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_86" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_87" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_88" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="0_xxMuted_89" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="52" layer="1" noteRangeFrom="48" noteRangeTo="52" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_00" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_01" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_02" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_03" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_04" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_05" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="54" layer="1" noteRangeFrom="53" noteRangeTo="54" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_06" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_07" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_08" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_09" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_10" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_11" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="56" layer="1" noteRangeFrom="55" noteRangeTo="56" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_12" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_13" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_14" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_15" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_16" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_17" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="58" layer="1" noteRangeFrom="57" noteRangeTo="58" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_18" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_19" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_20" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_21" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_22" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_23" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="60" layer="1" noteRangeFrom="59" noteRangeTo="60" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_24" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_25" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_26" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_27" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_28" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_29" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="62" layer="1" noteRangeFrom="61" noteRangeTo="62" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_30" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_31" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_32" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_33" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_34" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_35" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="64" layer="1" noteRangeFrom="63" noteRangeTo="64" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_36" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_37" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_38" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_39" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_40" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_41" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="66" layer="1" noteRangeFrom="65" noteRangeTo="66" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_42" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_43" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_44" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_45" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_46" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_47" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="68" layer="1" noteRangeFrom="67" noteRangeTo="68" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_48" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_49" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_50" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_51" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_52" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_53" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="70" layer="1" noteRangeFrom="69" noteRangeTo="70" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_54" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_55" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_56" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_57" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_58" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_59" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="72" layer="1" noteRangeFrom="71" noteRangeTo="72" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_60" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_61" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_62" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_63" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_64" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_65" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="74" layer="1" noteRangeFrom="73" noteRangeTo="74" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_66" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_67" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_68" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_69" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_70" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_71" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="76" layer="1" noteRangeFrom="75" noteRangeTo="76" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_72" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_73" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_74" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_75" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_76" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_77" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="78" layer="1" noteRangeFrom="77" noteRangeTo="78" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_78" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_79" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_80" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_81" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_82" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_83" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
<Sound centerNote="80" layer="1" noteRangeFrom="79" noteRangeTo="86" soundSet="2" xFade="false">
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_84" velocityRangeFrom="0.8" velocityRangeTo="1.0" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_85" velocityRangeFrom="0.68" velocityRangeTo="0.8" xFadeVelocityTo="1.0"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_86" velocityRangeFrom="0.52" velocityRangeTo="0.68" xFadeVelocityTo="0.8"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_87" velocityRangeFrom="0.36" velocityRangeTo="0.52" xFadeVelocityTo="0.68"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_88" velocityRangeFrom="0.2" velocityRangeTo="0.36" xFadeVelocityTo="0.52"/>
<SoundElement gainDb="0.0" rountRobinType="NONE" sample="1_xxMuted_89" velocityRangeFrom="0.0" velocityRangeTo="0.2" xFadeVelocityTo="0.36"/>
</Sound>
</Sounds>
</Patch>
</Patches>







<UI w="750" h="450">
<!--
<Element name="" x="" y="" w="" h="" param=""/>
<Element name="" x="" y="" w="" h=""/>
<Element name="" x="" y="" w="" h="" sizeX="" sizeY=""/>
-->
<Element name="LbPlayRangeShift" x="20" y="30" w="70" h="20" param="1PlayRange" visible="false" />
<Element name="LbKeySwitchRangeShift" x="90" y="30" w="70" h="20" param="2KSRange" visible="false" />
<Element name="LbMonoMode" x="160" y="30" w="70" h="20" param="3Mono" visible="false" />
<Element name="LbDfd" x="230" y="30" w="70" h="20" param="4Dfd" visible="false" />
<Element name="LbSoundSet" x="290" y="30" w="100" h="20" param="5Soundset" visible="false" />
<Element name="CbSoundSet" x="126" y="25" w="100" h="20" sizeX="100" sizeY="20" visible="true" />
<Element name="BuMonoMode" x="266" y="25" w="70" h="50" visible="true" />
<Element name="SlPlayRangeShift" x="341" y="25" w="70" h="50" visible="true" />
<Element name="SlKeySwitchRangeShift" x="416" y="25" w="70" h="50" visible="true" />
<Element name="BuDfd" x="230" y="50" w="70" h="50" visible="false" />
<Element name="SlTuneSt" x="486" y="25" w="70" h="50" visible="true" />
<Element name="SlTune" x="556" y="25" w="70" h="50" visible="true" />


<Element name="LbGain" x="50" y="110" w="70" h="20" param="1Gain" visible="false" />
<Element name="LbPan" x="580" y="30" w="70" h="20" param="2Pan" visible="false" />
<Element name="LbTune" x="610" y="30" w="70" h="20" param="3Tune" visible="false" />
<Element name="LbVelSens" x="680" y="30" w="70" h="20" param="4VSens" visible="false" />
<Element name="SlGain" x="25" y="105" w="70" h="50" visible="true" />
<Element name="SlPan" x="520" y="105" w="70" h="50" visible="true" />
<Element name="SlVelSens" x="590" y="105" w="70" h="50" visible="true" />
<Element name="SlBevel" x="660" y="105" w="70" h="50" visible="true" />

<Element name="LbPressureMode" x="20" y="110" w="100" h="20" param="1PressureMode" visible="false" />
<Element name="LbPressureController" x="120" y="110" w="100" h="20" param="2PressureCtrl" visible="false" />
<Element name="CbPressureMode" x="20" y="130" w="100" h="20" sizeX="100" sizeY="20" visible="false" />
<Element name="CbPressureController" x="120" y="130" w="100" h="20" sizeX="100" sizeY="20" visible="false" />

<Element name="LbLayer1Gain" x="470" y="110" w="70" h="20" param="1Gain1" visible="false" />
<Element name="LbLayer2Gain" x="540" y="110" w="70" h="20" param="2Gain2" visible="false" />
<Element name="LbLayer3Gain" x="610" y="110" w="70" h="20" param="3Gain3" visible="false" />
<Element name="LbLayer4Gain" x="680" y="110" w="70" h="20" param="4Gain4" visible="false" />
<Element name="SlLayer1Gain" x="95" y="105" w="70" h="50" visible="true" />
<Element name="SlLayer2Gain" x="165" y="105" w="70" h="50" visible="true" />
<Element name="SlLayer3Gain" x="610" y="130" w="70" h="50" visible="false" />
<Element name="SlLayer4Gain" x="680" y="130" w="70" h="50" visible="false" />


<Element name="LbPitchWheelMode" x="20" y="200" w="100" h="20" param="1PW_Mode" visible="false" />
<Element name="LbPitchWheelRange" x="120" y="200" w="70" h="20" param="2Range" visible="false" />
<Element name="LbPitchWheelRelease" x="190" y="200" w="70" h="20" param="3Release" visible="false" />
<Element name="LbPitchWheelOffset" x="260" y="200" w="70" h="20" param="4Offset" visible="false" />
<Element name="LbPitchWheelAttack" x="330" y="200" w="70" h="20" param="5Attack" visible="false" />
<Element name="CbPitchWheelMode" x="23" y="182" w="100" h="20" sizeX="100" sizeY="20" visible="true" />
<Element name="SlPitchWheelRange" x="168" y="182" w="70" h="50" visible="true" />
<Element name="SlPitchWheelRelease" x="25" y="253" w="70" h="50" visible="true" />
<Element name="SlPitchWheelOffset" x="95" y="253" w="70" h="50" visible="true" />
<Element name="SlPitchWheelAttack" x="165" y="253" w="70" h="50" visible="true" />


<Element name="LbLegatoMode" x="470" y="200" w="100" h="20" param="1LegatoMode" visible="false" />
<Element name="LbLegatoRelease" x="570" y="200" w="70" h="20" param="2Release" visible="false" />
<Element name="LbLegatoOffset" x="640" y="200" w="70" h="20" param="3Offset" visible="false" />
<Element name="LbLegatoAttack" x="710" y="200" w="70" h="20" param="4Attack" visible="false" />
<Element name="CbLegatoMode" x="522" y="182" w="100" h="20" sizeX="100" sizeY="20" visible="true" />
<Element name="SlLegatoRelease" x="520" y="253" w="70" h="50" visible="true" />
<Element name="SlLegatoOffset" x="590" y="253" w="70" h="50" visible="true" />
<Element name="SlLegatoAttack" x="660" y="253" w="70" h="50" visible="true" />


<Element name="LbEnvOffset" x="225" y="290" w="70" h="20" param="1Offset" visible="false" />
<Element name="LbEnvAttack" x="295" y="290" w="70" h="20" param="2Attack" visible="false" />
<Element name="LbEnvHold" x="365" y="290" w="70" h="20" param="3Hold" visible="false" />
<Element name="LbEnvDecay" x="435" y="290" w="70" h="20" param="4Decay" visible="false" />
<Element name="LbEnvSustain" x="505" y="290" w="70" h="20" param="5Sustain" visible="false" />
<Element name="LbEnvRelease" x="575" y="290" w="70" h="20" param="6Release" visible="false" />
<Element name="SlEnvOffset" x="168" y="340" w="70" h="50" visible="true" />
<Element name="SlEnvAttack" x="238" y="340" w="70" h="50" visible="true" />
<Element name="SlEnvHold" x="308" y="340" w="70" h="50" visible="true" />
<Element name="SlEnvDecay" x="378" y="340" w="70" h="50" visible="true" />
<Element name="SlEnvSustain" x="448" y="340" w="70" h="50" visible="true" />
<Element name="SlEnvRelease" x="518" y="340" w="70" h="50" visible="true" />

<Element name="CoMidiKeyboard" x="0" y="400" w="750" h="50" visible="true" />


</UI>




</Instrument>

)zzz";

#endif /* Configuration_h */
