/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/

/* (Auto-generated binary data file). */

#ifndef BINARY_UIIMAGES_H
#define BINARY_UIIMAGES_H

namespace UIImages
{
    extern const char*  knob_blue_png;
    const int           knob_blue_pngSize = 25769;

    extern const char*  knob_green_png;
    const int           knob_green_pngSize = 25503;

    extern const char*  knob_red_png;
    const int           knob_red_pngSize = 25801;

    extern const char*  knob_yellow_png;
    const int           knob_yellow_pngSize = 25718;

    extern const char*  toggle_blue_png;
    const int           toggle_blue_pngSize = 3432;

    extern const char*  toggle_green_png;
    const int           toggle_green_pngSize = 3031;

    extern const char*  toggle_red_png;
    const int           toggle_red_pngSize = 3024;

    extern const char*  toggle_yellow_png;
    const int           toggle_yellow_pngSize = 3295;

}

#endif
